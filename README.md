# PROJETO - BE THE HERO
## Semana Omnistack 11 - Rocketseat

Curso de React com NodeJS da Rocketseat.
O curso será dividido em 5 dias e criará um aplicativo do zero
completo.



## Primeiro Dia
  
* [x] Apresentar aplicação
* [x] Configurar ambiente de desenvolvimento
    * [x] Node.js & NPM
	* [x] Visual Studio Code
* [x] Entender sobre back-end & front-end
* [x] Criar projeto com Node.js
* [x] Entender sobre React & SPA
* [x] Criar projeto ReactJS
* [x] Entender sobre React Native & Expo

### Apresentando a Aplicação

A Aplicação da semana Omnistack 11 será um aplicativo chamado
Be The Hero, que é uma aplicação voltada à arrecadação de 
dinheiro para ONGs.

#### User Stories

* A Ong poderá fazer login na aplicação.
* A ONG poderá cadastrar os Casos que ela atende bem como adicionar
detalhes sobre esse caso, bem como, uma descrição e o custo daquele
caso.
* A ONG poderá disponibilizar o seu contato atravé de e-mail ou 
whatsapp.
* O Usuário poderá ver os casos.
* O usuário poderá entrar em contato com a ONG.

### Instalações

* Node
* React
* Express

### Extensões Visual Studio Code

* Material Icon Theme
* Dracula Theme

## Segundo Dia

* [x] Node.js & Express
    * [x] Rotas e recursos
    * [x] Métodos HTTP
    * [x] Tipos de parâmetros
* [x] Configurando o Nodemon
* [x] Utilizando o Insomnia
* Diferenças entre bancos de dados
* [ ] Configurando banco de dados
* [ ] Pensando nas entidades e funcionalidades
* [ ] Construção do back-end
* [ ] Adicionando módulo CORS
* [ ] Enviando o backend ao repositório remoto

### Rotas

Agora as rotas ficam no arquivo routes.js dentro da pasta src.

### Banco de Dados

Query Builder: KNEXJS
npm install knex
npm install sqlite3

### Entidades e Funcionalidades

#### Entidades 

Tudo aquilo que eu vou salvar no meu banco de dados.

* ONG
* Entidade: Caso (Incident)

#### Funcionalidades

* Login de ONG
* Logout de ONG
* Cadastro de ONG
* Cadastrar novos casos
* Deletar casos
* Listar casos específicos de uma ONG
* Listar todos os casos
* Entrar em contato com a ONG

#### Cors

npm install cors

## Terceiro Dia

* [x] Limpando a estrutura
* [x] Conceitos do React
    * [x] Componente
    * [x] JSX
    * [x] Propriedades
    * [x] Estado
    * [x] Imutabilidade
* [x] Página de Login
* [x] Configurando rotas
* [x] Cadastro de ONGs
* [x] Listagem de casos
* [x] Cadastro de um novo caso
* [x] Conectando aplicação à API
* [x] Enviar projeto ao repositório remoto

#### Instalações

npm install react-icons
npm install react-router-dom

## Quarto Dia

* [x] Instalando Expo
* [x] Criando projeto React Native
* [x] Executando projeto
    * [x] No celular
    * [x] Emuladores
    * [x] Expo Snack
* [x] Diferenças para o ReactJS
    * [x] Elementos HTML
    * [x] Semântica
    * [x] Estilização
        * [x] Flexbox
        * [x] Propriedades
        * [x]
        * [x] Estilização Própria
* [x] Estrutura de pastas
* [x] Ícone e Splash Screen
* [x] Configurando navegação
* [x] Página de casos
* [x] Detalhes do caso
* [x] Abrindo Whatsapp & E-mail
* [x] Conexão com a API
* [x] Enviando projeto ao repositório remoto

#### Instalações

npm install -g expo-cli

## Quinto Dia





# Informações adicionais

Site para montar o front end:
https://www.figma.com/